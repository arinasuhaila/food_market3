import 'package:equatable/equatable.dart';
import 'package:get/get.dart';

part 'food.dart';
part 'transaction.dart';
part 'user.dart';
